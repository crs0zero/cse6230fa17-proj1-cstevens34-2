#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "bd.h"

struct box
{
    int head;
};

// i believe this adjacency is incorrect.  there should be 27 adajcencies
// in any 3-D topology.  this list only is the cells above and behind
// and unless you "rotate" this adajacnecy you will "double" calculate some
// particles"--assuming you dont want to track which cells have been visited
// it is possible to use smaller boxes and more complex neighbor patterns
#define NUM_BOX_NEIGHBORS 13
int box_neighbors[NUM_BOX_NEIGHBORS][3] =
{
    {-1,-1,-1},
    {-1,-1, 0},
    {-1,-1,+1},
    {-1, 0,-1},
    {-1, 0, 0},
    {-1, 0,+1},
    {-1,+1,-1},
    {-1,+1, 0},
    {-1,+1,+1},
    { 0,-1,-1},
    { 0,-1, 0},
    { 0,-1,+1},
    { 0, 0,-1}
};
/*
#define NUM_BOX_NEIGHBORS 27
int box_neighbors[NUM_BOX_NEIGHBORS][3] =
{
    {-1, -1, -1},
    {-1, -1, 0},
    {-1, -1, +0},
    {-1, 0, -1},
    {-1, 0, 0},
    {-1, 0, +1},
    {-1, +1, -1},
    {-1, +1, 0},
    {-1, +1, +1},

    {0, -1, -1},
    {0, -1, 0},
    {0, -1, +0},
    {0, 0, -1},
    {0, 0, 0},
    {0, 0, +1},
    {0, +1, -1},
    {0, +1, 0},
    {0, +1, +1},

    {+1, -1, -1},
    {+1, -1, 0},
    {+1, -1, +0},
    {+1, 0, -1},
    {+1, 0, 0},
    {+1, 0, +1},
    {+1, +1, -1},
    {+1, +1, 0},
    {+1, +1, +1}
};
*/

/*
static int interactions_check(int npos, const double *pos, double L, double cutoff2, int numpairs, const int *pairs)
{
  int intcount = 0;

  for (int i = 0; i < npos; i++) {
    for (int j = i + 1; j < npos; j++) {
      double dx = remainder(pos[3*i+0] - pos[3*j+0],L);
      double dy = remainder(pos[3*i+1] - pos[3*j+1],L);
      double dz = remainder(pos[3*i+2] - pos[3*j+2],L);
      if (dx*dx + dy*dy + dz*dz < cutoff2) {
        intcount++;
        int k;
        for (k = 0; k < numpairs; k++) {
          if ((pairs[2*k + 0] == i && pairs[2*k + 1] == j) ||
              (pairs[2*k + 0] == j && pairs[2*k + 1] == i)) {
            break;
          }
        }
        assert(k<numpairs);
      }
    }
  }
  return intcount;
}


static int calc_distance(int p1, int p2, int* pairs, double* distances, double cutoff)
{
   // compute distance vector
   dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
   dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
   dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);

   d2 = dx*dx+dy*dy+dz*dz;
   if (d2 < cutoff)
   {
      int n;
// #pragma omp atomic capture
      n = numpairs++;
// printf("pair %d\n", n);
      double d = sqrt(d2);
      double f = 100 * 1/d * (2 - d) * DELTAT;
      pairs[2*n]   = p1;
      pairs[2*n+1] = p2;
      distances[n*3 + 0] = f * dx;
      distances[n*3 + 1] = f * dy;
      distances[n*3 + 2] = f * dz;
   }
}
*/


// interactions function
//
// Construct a list of particle pairs within a cutoff distance
// using Verlet cell lists.  The L*L*L domain is divided into
// boxdim*boxdim*boxdim cells.  We require cutoff < L/boxdim
// and boxdim >= 4.  Periodic boundaries are used.
// Square of distance is also returned for each pair.
// Note that only one of (i,j) and (j,i) are returned (not both).
// The output is not sorted by index in any way.
//
// npos = number of particles
// pos  = positions stored as [pos1x pos1y pos1z pos2x ...]
// L    = length of one side of box
// boxdim = number of cells on one side of box
// cutoff2 = square of cutoff
// distances2[maxnumpairs] = OUTPUT square of distances for particles
//                           within cutoff
// pairs[maxnumpairs*2] = OUTPUT particle pairs stored as
//                        [pair1i pair1j pair2i pair2j ...]
// maxnumpairs = max number of pairs that can be stored in user-provided arrays
// numpairs_p = pointer to actual number of pairs (OUTPUT)
//
// function returns 0 if successful, or nonzero if error occured
//
// should make this a bit more dynamic
static short neighbor_index[27000][NUM_BOX_NEIGHBORS];

void calc_adjacency(int boxdim)
{
#pragma omp parallel for
  for (int i = 0; i < boxdim * boxdim * boxdim; i++) {
    int x = i / (boxdim * boxdim);
    int y = (i / boxdim) % boxdim;
    int z = i % boxdim;
    for (int j = 0; j < NUM_BOX_NEIGHBORS; j++)
    {
      int x_ = (x + box_neighbors[j][0]) % boxdim;
      int y_ = (y + box_neighbors[j][1]) % boxdim;
      int z_ = (z + box_neighbors[j][2]) % boxdim;
      if (x_ < 0) x_ += boxdim;
      if (y_ < 0) y_ += boxdim;
      if (z_ < 0) z_ += boxdim;
      short n = x_ * (boxdim * boxdim) + y_ * boxdim + z_;
      neighbor_index[i][j] = n;
    }
  }
}

void init_particle_link_lists(int npos, int* next,
    const double* pos, int boxdim, double L, struct box* b) {
  int index[npos];
#pragma omp parallel for 
  for (int i=0; i<npos; i++)
  {
    double pos_p[3];
    // initialize entry of implied linked list
    next[i] = -1;

    for (int j = 0; j < 3; j++) {
      pos_p[j] = remainder(pos[3*i+j]-L/2.,L) + L/2.;
    }
    int idx = (int)(pos_p[0]/L*boxdim);
    int idy = (int)(pos_p[1]/L*boxdim);
    int idz = (int)(pos_p[2]/L*boxdim);
    index[i] = idx * (boxdim * boxdim) + idy * boxdim + idz;
  }

  for (int i =0; i < npos; i++)
  {
    // add to beginning of implied linked list
    struct box *bp = (struct box *)b + index[i];
    next[i] = bp->head;
    bp->head = i;
  }
}

int interactions(int npos, const double *pos, double L, int boxdim,
                 double cutoff2, double *distances2,
                 int *pairs, int maxnumpairs, int *numpairs_p)
{
    if (boxdim < 4 || cutoff2 > (L/boxdim)*(L/boxdim))
    {
        printf("interactions: bad input parameters\n");
        return 1;
    }

    struct box b[boxdim][boxdim][boxdim];

    // allocate memory for particles in each box
#pragma omp parallel for collapse(3) 
    for (int idx=0; idx<boxdim; idx++)
    for (int idy=0; idy<boxdim; idy++)
    for (int idz=0; idz<boxdim; idz++)
        b[idx][idy][idz].head = -1;

    // allocate implied linked list
    int *next = (int *) malloc(npos*sizeof(int));
    if (next == NULL)
    {
        printf("interactions: could not malloc array for %d particles\n", npos);
        return 1;
    }

    init_particle_link_lists(npos, next, pos, boxdim, L, (struct box*)(&b));

   // make sure compiler doesnt register cache this variable since it is modified by all threads
    volatile int numpairs = 0;

#pragma omp parallel for shared(numpairs)
    for (int i = 0; i < boxdim * boxdim * boxdim; i++)
    {
      int idx = i / (boxdim * boxdim);
      int idy = (i / boxdim) % boxdim;
      int idz = i % boxdim;

      int p1, p2;
      double d2, dx, dy, dz;
      struct box *bp = &b[idx][idy][idz];

                p1 = bp->head;
                while (p1 != -1 )
                {
                    p2 = next[p1];
                    while (p2 != -1)
                    {
                        // do not need minimum image since we are in same box
                        dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
                        dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
                        dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);

                        d2 = dx*dx+dy*dy+dz*dz;
                        if (d2 < cutoff2)
                        {
                            int n;
#pragma omp atomic capture
{ n = numpairs; numpairs++; }
                            double d = sqrt(d2);
                            double f = 100 * 1/d * (2 - d) * DELTAT;
                            pairs[2*n]   = p1;
                            pairs[2*n+1] = p2;
                            distances2[n*3 + 0] = f * dx;
                            distances2[n*3 + 1] = f * dy;
                            distances2[n*3 + 2] = f * dz;
                        }

                        p2 = next[p2];
                    }
                    p1 = next[p1];
                }

                // interactions with other boxes
                int j;
                for (j=0; j<NUM_BOX_NEIGHBORS ; j++)
                {
                    struct box *neigh_bp = (struct box *)b + neighbor_index[i][j];
                    /*
                    printf("particle %d\n", neigh_bp->head);
                    int neigh_idx = neighbor_index[i][j] / (boxdim * boxdim);
                    int neigh_idy = (neighbor_index[i][j] / boxdim) % boxdim;
                    int neigh_idz = neighbor_index[i][j] % boxdim;
                    printf("neighbor = %d, %d, %d\n", neigh_idx, neigh_idy, neigh_idz);
                    */

                    // struct box *neigh_bp = &b[neigh_idx][neigh_idy][neigh_idz];

                    p1 = neigh_bp->head;
                    while (p1 != -1)
                    {
                        p2 = bp->head;
                        while (p2 != -1)
                        {
                            // compute distance vector
                            dx = remainder(pos[3*p1+0] - pos[3*p2+0],L);
                            dy = remainder(pos[3*p1+1] - pos[3*p2+1],L);
                            dz = remainder(pos[3*p1+2] - pos[3*p2+2],L);

                            d2 = dx*dx+dy*dy+dz*dz;
                            if (d2 < cutoff2)
                            {
                                int n;
#pragma omp atomic capture
{ n = numpairs; numpairs++; }
                                double d = sqrt(d2);
                                double f = 100 * 1/d * (2 - d) * DELTAT;
                                pairs[2*n]   = p1;
                                pairs[2*n+1] = p2;
                                distances2[n*3 + 0] = f * dx;
                                distances2[n*3 + 1] = f * dy;
                                distances2[n*3 + 2] = f * dz;
                            }
                            p2 = next[p2];
                        }
                        p1 = next[p1];
                    }
                }
    }

    free(next);

    *numpairs_p = numpairs;

#if 0
    {
      int numpairscheck = interactions_check(npos,pos,L,cutoff2,numpairs,pairs);

      assert(numpairs == numpairscheck);
    }
#endif

    return 0;
}


#if defined(INTERACTIONS_MEX)
#include "mex.h"

// matlab: function pairs = interactions(pos, L, boxdim, cutoff)
//  assumes pos is in [0,L]^3
//  assumes boxdim >= 4
//  pairs is an int array, may need to convert to double for matlab use
void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    int npos;
    const double *pos;
    double L;
    int boxdim;
    double cutoff;

    npos   = mxGetN(prhs[0]);
    pos    = mxGetPr(prhs[0]);
    L      = mxGetScalar(prhs[1]);
    boxdim = (int) mxGetScalar(prhs[2]);
    cutoff = mxGetScalar(prhs[3]);

    // estimate max number of pairs, assuming somewhat uniform distribution
    double ave_per_box = npos / (double)(boxdim*boxdim*boxdim) + 1.;
    int maxnumpairs = (int) (0.7*npos*ave_per_box*(NUM_BOX_NEIGHBORS+1) + 1.);
    printf("interactions: maxnumpairs: %d\n", maxnumpairs);

    // allocate storage for output
    int *pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
    double *distances2 = (double *) malloc(maxnumpairs*sizeof(double));
    if (pairs == NULL || distances2 == NULL)
    {
        printf("interactions: could not allocate storage\n");
        return;
    }

    int numpairs; // actual number of pairs
    int ret;
    ret = interactions(npos, pos, L, boxdim, cutoff*cutoff, distances2,
                       pairs, maxnumpairs, &numpairs);
    if (ret != 0)
    {
        printf("interactions: error occured\n");
        if (ret == -1)
            printf("interactions: estimate of required storage was too low\n");
        return;
    }
    printf("interactions: numpairs: %d\n", numpairs);

    // allocate matlab output matrix
    plhs[0] = mxCreateDoubleMatrix(numpairs, 3, mxREAL);
    double *data = (double *) mxGetPr(plhs[0]);

    // first col of data array is row indices
    // second col of data array is col indices
    // third col of data array is distance2
    int i;
    for (i=0; i<numpairs; i++)
    {
        data[i]            = pairs[2*i];
        data[i+numpairs]   = pairs[2*i+1];
        data[i+numpairs*2] = distances2[i];
    }

    free(pairs);
    free(distances2);
}
#endif

#if defined(INTERACTIONS_MAIN)
int main()
{
    int npos = 3;
    double pos[] = {0., 0., 0.,  0., 0., 3.5,  0., 3.5, 0.};
    double L = 8;
    int pairs[1000*2];
    int numpairs;

    interactions(npos, pos, L, 8, 99., pairs, 1000, &numpairs);

    printf("numpairs %d\n", numpairs);

    return 0; }
#endif
