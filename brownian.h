#include <stdlib.h>
#include <string.h>

#define PI 3.1415926535897932
// Array constructs
typedef struct 
{
    void* values;
    int size;
    int capacity;
} Array;

inline void create_array(Array* a, int sizeOf, int capacity)
{
    a->values = malloc(sizeOf * capacity);
    a->size = 0;
    a->capacity = capacity;
}

inline void resize_array(Array* a, int sizeOf, int capacity)
{
    a->values = realloc(a->values, sizeOf * capacity);
    a->capacity = capacity;
}

/*================================================================================*/

typedef struct 
{
    double x,y,z;
} Point;

typedef struct 
{
    Point position;
    double force;
} Particle;

inline void apply_force(Particle* p, double f) { p->force += f; }
inline void displace(Particle* p) 
{ 
    p->position.x += p->force;
    p->position.y += p->force;
    p->position.z += p->force;
    p->force = 0;
}

/*================================================================================*/
// Array definitions

typedef struct 
{
    Particle* values;
    int size;
    int capacity;
} ParticleArray;

typedef struct 
{
    int* values;
    int size;
    int capacity;
} IntArray;

struct _Cell;
typedef struct 
{
    struct _Cell* values;
    int size;
    int capacity;
} CellArray;

/*================================================================================*/

typedef struct _Cell
{
    IntArray particles;
    IntArray adjacency;
} Cell;

/*
inline void cell_add_particle(Cell* cell, Particle* p) { cell->particles.values[cell->particles.size++] = p; }
inline Particle* cell_get_particle(Cell* cell, int n) { return (Particle*)cell->particles.values[n]; }
*/


/*================================================================================*/

typedef struct 
{
    ParticleArray particles;
    CellArray cells;
    int64_t dimensions;     // vector coordinate dimensions (w,h,l)
    double width;           // spatial coordinate dimension of the box (w,h,l)
    double cell_width;      // spatial coordinate dimension of a cell
} Box;


inline Particle* add_particle(Box* b)
{
    Particle* p = &b->particles.values[b->particles.size++];
    return p;
}

void create_box(Box* b, double width, int size, int is_torus);
void step(Box* box);
void gather_pairs(Box* box, IntArray* neighbors);

inline void run(Box* box, int time)
{
    // cannot parallelize this loop since each time step is dependant on the next
    for (int t = 0; t < time; t++)
        step(box);
}


