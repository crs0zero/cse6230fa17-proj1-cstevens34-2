#include "brownian.h"
#include <math.h>

void create_box(Box* b, double width, int size, int is_torus)
{
    printf("creating box\n");
    create_array((Array*)&b->particles, sizeof(Particle), size);

    b->width = width;

    b->dimensions = ceil(width/2);
    b->cell_width = 2;
    // b->cell_width = dimensions/b->dimensions;

    create_array((Array*)&b->cells, sizeof(Cell), b->dimensions * b->dimensions * b->dimensions);
    for (int i = 0; i < b->cells.capacity; i++)
    {
        create_array((Array*)&b->cells.values[i].particles, sizeof(int), 4);
        create_array((Array*)&b->cells.values[i].adjacency, sizeof(int), 27);
    }
    b->cells.size = b->cells.capacity;

    char visited[b->cells.size];
    memset(visited, 0, b->cells.size);

    // precalculate the list of adjacent cells
    // this step can be precalculated since the list of adjacent cell is static
    // gather work step
    // this step is single threaded since there is contention amongst resources
    // the main contention is the calculation of which particles have already
    // been visited i.e. which particles have had (or will have) a complete force 
    // calculation performed.  We want to ensure that we do not duplicate the 
    // calculation on these particles.  During this step, each cell is visited in 
    // order, and then the cell (and the particles it contains) is marked as 
    // "visited" which means that subsequent iterations will not pull those cells 
    // and hence the particles within that cell.
    int i = 0;
    for (int z = 0; z < b->dimensions; z++)
    for (int y = 0; y < b->dimensions; y++)
    for (int x = 0; x < b->dimensions; x++)
    {
        for (int zp = -1; zp < 2; zp ++)
        for (int yp = -1; yp < 2; yp ++)
        for (int xp = -1; xp < 2; xp ++)
        {
            int z_, y_, x_;

            if (is_torus)
            {
                z_ = (z + zp) % b->dimensions;
                y_ = (y + yp) % b->dimensions;
                x_ = (x + xp) % b->dimensions;
            }
            else
            {
                z_ = z + zp;
                y_ = y + yp;
                x_ = x + xp;
            }

            /* I think your method doesn't take into account cells
             * that are adjacent accross periodic boundaries */

            // why would we do that??  I know of no substance that actually 
            // exists as a torus which would be the only plausible geometric 
            // relationship that would account for that type of periodicity.  
            // My orginal search on brownian motion only referenced regular 
            // particle dynamics and I did not think to explore polymers and 
            // such (which have some scholarly articles relating to brownian 
            // simulations).  This is expressely why I ask for a reference 
            // problem description which was not provided.
            if (z_ >= 0 && z_ < b->dimensions && 
                y_ >= 0 && y_ < b->dimensions && 
                x_ >= 0 && x_ < b->dimensions)
            {
                int n = z_ * (b->dimensions * b->dimensions) + y_ * b->dimensions + x_;
                if (!visited[n])
                {
                    Cell* cell = &b->cells.values[n];
                    cell->adjacency.values[cell->adjacency.size++] = n;
                }
            }
        }
        visited[i++] = 1;
    }
}


// __thread Random *Box::_rng;
//
void step(Box* box)
{
    // int size = 0;
    // if (debug > 0) std::cout << "clear cells" << std::endl;

    // assign the particles 
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < box->cells.size; i++)
    {
        box->cells.values[i].particles.size = 0;
    }

    // printf("assigning particles\n");
    #pragma omp parallel for simd schedule(static)
    for (int i = 0; i < box->particles.size; i++)
    {
        int x= (remainder(box->particles.values[i].position.x - box->dimensions/2, box->dimensions) +  box->dimensions/2) / box->cell_width;
        int y= (remainder(box->particles.values[i].position.y - box->dimensions/2, box->dimensions) +  box->dimensions/2) / box->cell_width;
        int z= (remainder(box->particles.values[i].position.z - box->dimensions/2, box->dimensions) +  box->dimensions/2) / box->cell_width;

        Cell* cell = &box->cells.values[z * box->dimensions * box->dimensions + y * box->dimensions + x];

        if (cell->particles.capacity == cell->particles.size)
            resize_array((Array*)&cell->particles, sizeof(int), cell->particles.size * 2);
        cell->particles.values[cell->particles.size++] = i;
    }

    // printf("gather particles\n");
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < box->cells.size; i++)
    {
        gather_pairs(box, &box->cells.values[i].adjacency);
    }

/*
    #pragma omp parallel for simd schedule(static)
    for (int i = 0; i < box->particles.size; i++)
    {
        // add the guassian noise to particle movement.  Not sure what this should
        // be so a mean of 0 and deviation of 1 seems reasonable.
        // NormalDistribution distribution(0, 1);
        // const double DT = sqrt(2.0 * 1E-4);
        const double DT = 0.014142135623731;
        double f = distribution(*_rng);
        apply_force(box->particles.values[i], DT*f);
        displace(box->particles.values[i]);
    }
    */
}


__thread IntArray particles;

void gather_pairs(Box* box, IntArray* neighbors)
{
    // gather particle pairing step.
    // this step collects all the particles from the current cell and all adjacent cells
    // then it computes the pairings of those particles for later force calculation
    //
    // fetch a pre-allocated vector of particles.  This helps optimize memory allocations over
    // repeated steps.
    //
    //
    for int i = 0; i < box->dimensions*box->dimensions*box->dimensions; i++)
        int pId = cell[x][y][z];
        Particle* p = &box->particles.values[pId]
            pId = next[pId]

    if (particles.capacity == 0) 
        create_array((Array*)&particles, sizeof(int), 32);
    particles.size = 0;
    for (int i = 0; i < neighbors->size; i++)
    {
        Cell* cell = &box->cells.values[neighbors->values[i]];
        for (int j =0; j < cell->particles.size; j++)
        {
            if (particles.capacity == particles.size)
                resize_array((Array*)&particles, sizeof(int), particles.size * 2);
            particles.values[particles.size++] = cell->particles.values[j];
        }
    }

// #pragma omp section simd
    for (int i = 0; i < particles.size; i++)
        for (int j = i + 1; j < particles.size; j++)
        {
            Particle* p1 = &box->particles.values[particles.values[i]];
            Particle* p2 = &box->particles.values[particles.values[j]];

            // calculate the distance between particles
            double dx = remainder(p1->position.x - p2->position.x, box->dimensions);
            double dy = remainder(p1->position.y - p2->position.y, box->dimensions);
            double dz = remainder(p1->position.z - p2->position.z, box->dimensions);

            // determine force to be applied based on distance
            double dt = (dx * dx) + (dy * dy) + (dz * dz);
            if (dt < 4)
            {
                double distance = sqrt(dt);
                double f = 100*(2-distance)/distance;
                apply_force(p1, distance * f);
                apply_force(p2, distance * -f);
            }
        }
}

