#include <iostream>
#include <iomanip>
#include <fstream>
#include "random.h"

int main(int argc, char* argv[])
{
	int seed = 23;
	int size = 10000;
    double box_width, volume_fraction;
    FILE *output = 0;
    Random rng;

	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			switch(argv[i][1])
			{
                case 'n': size = atoi(argv[++i]); break;
                case 'o': output = fopen(argv[++i], "w"); break;
				case 'r': seed = atoi(argv[++i]); break;
				case 'v': volume_fraction = atof(argv[++i]); break;
				default:
					std::cout<< "unrecognized option" << argv[i] << std::endl;
			}
		}
		else std::cout<< "unrecognized option" << argv[i] << std::endl;
	}

    if (!output)
    {
        std::cout << "Usage: " << argv[0] << "-n <size> -o <file> -r <seed>\n";
        std::cout << "\t-o: set the outpu file" << std::endl;
        std::cout << "\t-n: size of the simulation" << std::endl;
        std::cout << "\t-r: set the random seed (if used) for the program" << std::endl;
        std::cout << "\t-v: set the volume fraction" << std::endl;
        std::cout << std::endl;
        exit(0);
    }

    box_width = cbrt(4.0/3.0 * M_PI * size / volume_fraction);
    UniformDistribution distribution(0, box_width);

    fprintf(output, "%d\n", size);
    fprintf(output, "%7f %7f\n", 0.0, box_width);
    for (int i = 0; i < size; i++)
    {
        double x = distribution(rng);
        double y = distribution(rng);
        double z = distribution(rng);
        fprintf(output, "0 %7f %7f %7f\n", x, y, z);
    }

    fclose(output);
}
