#include <iostream>
#include <ctime>
#include <cstdlib>
#include <limits>
#include <sys/time.h>
#include <omp.h>
#include <fstream>
#include "box.h"
#include <time.h>

extern int debug;

#define INTERVAL_LEN 1000
#define DELTAT       1e-4


int64_t time()
{
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    int64_t time = tp.tv_sec * 1000000;
    time += tp.tv_nsec/1000;

    return time;
}

int main(int argc, char* argv[])
{
	int size = 10000;
	int intervals = 5;
    int threads = 1;
    bool r = false;
    int seed = 23;
    double box_width, start_time;

    std::ifstream input;
    std::ofstream output;

    int pos = 0;

	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			switch(argv[i][1])
			{
				case 'd': debug = atoi(argv[++i]); break;
				case 'i': intervals = atoi(argv[++i]); break;
                case 'f': // in file
                          input.open(argv[++i]);
                          break;
                case 'n': size = atoi(argv[++i]); break;
                case 'o': // out file
                          output.open(argv[++i]);
                          break;
                case 'r': r = true;
                          seed = atoi(argv[++i]); 
                          break;
                case 't' : threads = atoi(argv[++i]); break;
				default:
					std::cout<< "unrecognized option" << argv[i] << std::endl;
			}
		}
		else std::cout<< "unrecognized option" << argv[i] << std::endl;
	}

    if (!input.is_open() || !output.is_open())
    {
        std::cout << "Usage: " << argv[0] << "-n <size> -t <threads> -i <intervals> -r <seed>\n";
        std::cout << "\t-f: set the input file" << std::endl;
        std::cout << "\t-o: set the output file" << std::endl;
        std::cout << "\t-i: number of intervals (time) to run the program" << std::endl;
        std::cout << "\t-n: size of the simulation" << std::endl;
        std::cout << "\t-r: set the random seed (if used) for the program" << std::endl;
        std::cout << "\t-t: number of threads to run in parallel" << std::endl;
        std::cout << std::endl;
        exit(0);
    }

    omp_set_num_threads(threads);

    int type;
    // read the input file;
    input >> size;
    input >> start_time;
    input >> box_width;
    for (char ch=0; ch != '\n'; input.get(ch));

    Box& box = *new Box(Point(box_width, box_width,  box_width), 1);

    box.init(size, seed);

    for (int i = 0; i < size; i++)
    {
        Particle p;
        input >> type;
        input >> p.position.x;
        input >> p.position.y;
        input >> p.position.z;

	// printf("adding particle\n");
        box.add_particle(p);
    }

    std::cout << "Number of particles: " << size << std::endl;
    std::cout << "Number of intervals to simulate: " << intervals << std::endl;
    std::cout << "Using random seed: " << seed << std::endl;
    std::cout << "Using thread count: " << threads << std::endl;

    // initialize random number
    const double volume_constant = (4.0*M_PI)/3.0;
    double volume_fraction = (volume_constant * size) / (box_width*box_width*box_width);

    // seems wrong but not sure what this is used for--interaction volume is constant regardless of size and interactions per particle is just 8 times diffusion-volume.  pretty sure physics doesnt work like this.
    double interaction_volume = 8 * volume_constant;
    double interactions_per_particle = (size * interaction_volume) / (box_width*box_width*box_width);

    printf("With %d particles of radius 1 and a box width of %f, the volume fraction is %g.\n",size,box_width,volume_fraction);
    printf("The interaction volume is %g, so we expect %g interactions per particle, %g overall.\n",interaction_volume,interactions_per_particle,interactions_per_particle * size / 2.);
    // printf("Allocating space for %ld interactions\n",n);

    int64_t before_sim_timestamp = time();
    for (int i = 0; i < intervals; i++)
    {
        int64_t begin = time();

        box.simulate(1000);
        
        int64_t finish = time();
        int64_t run_time = finish - begin;
        int64_t cum_time = finish - before_sim_timestamp;
        std::cout << "time: " << run_time/1000000.0
          << ", " << cum_time/1000000.0 << std::endl;
        if (i % 5 == 0)
          std::cout << "Done interval: " << i << std::endl;

        //write frame
        output << size << std::endl;
        output << start_time+i*INTERVAL_LEN*DELTAT << " " << box_width << std::endl;
        std::vector<Particle>& particles = box.get_particles();
        for (std::vector<Particle>::iterator i = particles.begin() + 1; i != particles.end(); i++) {
          Particle& p = *i;
          output << "0 " //the type is always zero, for now
            << p.position.x << " "
            << p.position.y << " "
            << p.position.z << std::endl;
        }
    }
    int64_t after_sim_timestamp = time();
    int64_t microseconds = after_sim_timestamp - before_sim_timestamp;
    double seconds = microseconds/1000000.0;
    std::cout << "Time: " << seconds << " for " << intervals << " intervals" << std::endl;
    exit(0);
}
