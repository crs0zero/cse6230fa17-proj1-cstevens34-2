#!/bin/sh
#SBATCH --job-name=proj1_t32_i20   # Job name
#SBATCH --time=00:10:00               # Time limit hrs:min:sec
#SBATCH --output=proj1_t32_i20_%j.out    # Standard output and error log
##SBATCH --mail-type=NONE               # Mail events (NONE, BEGIN, END, FAIL, ALL)
##SBATCH --mail-user=<email_address>   # Where to send mail  

cd ~/cse6230fa17-proj1-cstevens34/
pwd; hostname; date
time make NTHREADS=32 NINTERVALS=20 JOBID="$SLURM_JOB_ID" runharness
date
