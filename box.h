#if !defined (BOX_H)
#define BOX_H

#include "cell.h"
#include <vector>
#include <utility>
#include <map>
#include "random.h"

// struct ParticlePair { int p1; int p2; double force; ParticlePair(int part1, int part2, int force) : }
// typedef std::pair<unsigned short, unsigned short> ParticlePair;
// typedef std::vector< ParticlePair > PairArray;

class Box
{
public:

    Box(Point dimensions, float particle_radius) ;

    void init(int size, int seed);

    inline void add_particle(Particle& p)
    {
        int x = (remainder(p.position.x - _dimensions/2, _dimensions) + _dimensions/2)/_celldim;
        int y = (remainder(p.position.y - _dimensions/2, _dimensions) + _dimensions/2)/_celldim;
        int z = (remainder(p.position.z - _dimensions/2, _dimensions) + _dimensions/2)/_celldim;

        p.id = _particles.size();
        _particles.push_back(p);
        (*this)(x,y,z).add_particle(&p);
    }

/*
    inline bool contains(const Particle& p)
    {
        return 
            (p.position.x >= 0 && p.position.x < _dimensions) &&
            (p.position.y >= 0 && p.position.y < _dimensions) && 
            (p.position.z >= 0 && p.position.z < _dimensions);
    }
*/

    inline int x(int i)
    {
        return i % _cols;
    }
    inline int y(int i)
    {
        return (i / _cols) % _rows;
    }

    inline int z(int i)
    {
        return i / (_rows * _cols);
    }

    inline int get_index(int col, int row, int level)
    {
        return (level * _cols * _rows) + (row * _cols) + col;
    }

    inline Cell& operator ()(int col, int row, int level)
    {
        return _cells[get_index(col, row, level)];
    }

    void simulate(int time);
    void step();
    inline std::vector<Particle>& get_particles() {
      return _particles;
    }

    // void gather_pairs(int &size, std::vector<int>& cells);
    void gather_pairs(std::vector<int>& cells);

private:
    std::vector<Cell> _cells;
    std::vector<Particle> _particles;
    double _dimensions; // box dimensions x = y = z
    double _celldim; // cell dimensions x = y = z
    // Point _dimensions; Point _celldim;
    int _cols; // = x 
    int _rows; // = y 
    int _levels; // = z 

    // bookkeeping members--these are here to make sure that memory allocations only occur once
    // PairArray pairs;
    std::vector< std::vector<int> > worklist;
    std::map< int, ParticleVector > _vectors;
    static __thread Random *_rng;
};


#endif 
