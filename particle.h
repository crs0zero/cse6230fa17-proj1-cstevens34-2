#if !defined (PARTICLE_H)
#define PARTICLE_H

#include <cmath>
#include <stdint.h>
#include <vector>

struct Point
{
    double x, y, z;

    inline Point(double i, double j, double k) : x(i), y(j), z(k) {}
    inline double distance(Point p)
    {
        return sqrt( (p.x - x)*(p.x - x) + (p.y - y)*(p.y - y) + (p.z - z)*(p.z - z) );
    }
};

class Particle
{
public:
    int id;
    int cell;
    // int type;
    Point position;  // the position of the particle

    inline Particle() : position(0,0,0), id(0), _force(0,0,0)
    {
    }

    #pragma omp declare simd inbranch
    inline void apply_force(double force)
    {
        #pragma omp atomic
        _force.x += force;
        #pragma omp atomic
        _force.y += force;
        #pragma omp atomic
        _force.z += force;
    }

/*
    // #pragma omp declare simd inbranch
    inline void apply_force_branched(double force)
    {
        #pragma omp atomic
        _force.x += force;
        #pragma omp atomic
        _force.y += force;
        #pragma omp atomic
        _force.z += force;
    }
*/

    #pragma omp declare simd inbranch
    inline void displace()
    {
        position.x += _force.x;
        position.y += _force.y;
        position.z += _force.z;
        _force.x = _force.y = _force.z = 0;
    }

private:
    Point _force;     // the force being applied during this time step
};
typedef std::vector<Particle*> ParticleVector;
/*
struct ParticlePair 
{ 
	Particle *first, *second; 
	double distance; 
	ParticlePair() : first(0), second(0), distance(0) {}
	ParticlePair(Particle *p1, Particle* p2, double d) : first(p1), second(p2), distance(d) {}
	ParticlePair& operator=(const ParticlePair& p) { first = p.first; second = p.second; distance = p.distance; }
 };
*/

#endif
