#include <cstdio>
#include <omp.h>
#include "memfile.h"

    void MemFile::print(char* format, ...)
    {
        char buffer[1024];
        va_list args;
        va_start (args, format);
        int n = vsnprintf (buffer, 1024, format, args);
        write(buffer, n);
        va_end (args);
    }

    int MemFile::write(const char* buffer, int len)
    {
        char* buff = new char[len];
        memcpy(buff, buffer, len);
#pragma omp critical
        {
            _sections.push_back(Section(_offset, buff));
            _offset += len;
        }
    }

