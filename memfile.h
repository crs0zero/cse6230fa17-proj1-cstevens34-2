#if !defined(MEMFILE_H)
#define MEMFILE_H

#include <string>
#include <list>
#include <stdint.h>

class MemFile
{
public:

    inline MemFile() : _offset(0) { }

    inline MemFile& operator <<(char c) { write(&c, 1); return *this; }
    inline MemFile& operator <<(char* str) { write(str, strlen(str)); return *this; }
    inline MemFile& operator <<(std::string&  str) { write(str.c_str(), str.length()); return *this; }

    template <typename T> inline void itoa(T val, char* s)
    {
        int n = 0;
        int offset = 0;
        char *ptr = s;
        if (val < 0)
        {
            *ptr = '-';
            ptr++;
            val = -val;
            offset = 1;
        }
        do
        {
            *ptr = "0123456789"[(val%10)];
            // *s = (val % 10) + '0';
            val /= 10;
            n++;
        } while (val > 0);

        for (int i = 0; i < n/2; i++)
            std::swap(s[i+offset], s[offset+(n-i-1)]);
    }

    inline MemFile& operator <<(int  i) { 
        char s[50];
        itoa(i, s);
        *this << s;
        return *this;
    }

    inline MemFile& operator <<(int64_t  i) { 
        char s[50];
        itoa(i, s);
        *this << s;
        return *this;
    }

    void open(std::string name)
    {
        _filename = name;
    }
    void print(char* format, ...);
    
private:
    typedef std::pair<int64_t, char*> Section;

    int write(const char* buffer, int len);

    int64_t _offset;
    std::string _filename;
    std::list<Section> _sections;
};


#endif
