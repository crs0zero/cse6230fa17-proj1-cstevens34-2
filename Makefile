DIR := /nethome/wmartin45/.linuxbrew
export PATH := $(DIR)/bin:$(PATH)
CC = $(DIR)/bin/gcc-5
CSE6230UTILSDIR = ./utils
# NOTE: on deepthought with the gnu4.4 compiler, you will have to add -std=gnu99 for harness to compile 
# correctly
#CFLAGS = -g -Wall -O3 -std=gnu99
CFLAGS = -Wall -O3 -I$(DIR)/include -std=gnu99 
CPPFLAGS = -I$(CSE6230UTILSDIR) -I$(CSE6230UTILSDIR)/tictoc
OMPFLAGS = -fopenmp -L$(DIR)/lib
LIBS = -lm
RM = rm -f
#INFILE = lac1_novl2.xyz
INFILE = data.xyz
OUTFILE = out.xyz
NINTERVALS = 20
HARNESS_ENV =

all: runharness

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(OMPFLAGS) -c -o $@ $<

harness: harness.o bd.o interactions.o
	$(CC) $(OMPFLAGS) -o $@ $^ $(LIBS)

runharness: harness
	$(RM) $(OUTFILE) && $(HARNESS_ENV) ./harness $(INFILE) $(OUTFILE) $(NINTERVALS)

clean:
	$(RM) *.o harness

.PHONY: clean runharness
