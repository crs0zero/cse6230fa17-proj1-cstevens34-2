cstevens34
	Compile/Output: 1 (1- The format of output file was changed. The changed format didn't have the end times for every interval. 2- Path had to be changed for getting the code to compile.)
	Source: 3 (1- The computed diffusion coefficient was close to 9.1 which was well outside the acceptable range of 0.7-0.9. 2- Please see the comments added in the source code.)
	Speed: 1
	Report: 3 (Please see 'instructor-notes.txt' for additional feedback) 
### Graded by Ankit

